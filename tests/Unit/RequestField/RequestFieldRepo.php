<?php

namespace Tests\Unit\RequestField;

use Webmagic\Request\RequestField\RequestFieldRepo as BaseRepo;

class RequestFieldRepo extends BaseRepo
{
    public function setEntity($entity_name)
    {
        $this->entity = $entity_name;
    }
}