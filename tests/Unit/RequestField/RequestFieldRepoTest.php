<?php

use Tests\TestCase;
use Tests\Unit\RequestField\RequestFieldRepo;
use Webmagic\Request\RequestField\RequestField;
use Webmagic\Request\RequestType\RequestType;

class RequestFieldRepoTest extends TestCase
{

    /** @var  RequestFieldRepo */
    protected $repo;

    public function setUp()
    {
        parent::setUp();

        $this->repo = new RequestFieldRepo();
        $this->repo->setEntity(RequestField::class);
    }



    public function testGetByTypeId()
    {
        $type = factory(RequestType::class)->create();

        //Check instance of empty collection
        $fields = $this->repo->getByTypeID($type->id);
        $this->assertTrue($fields instanceof \Illuminate\Database\Eloquent\Collection);
        $this->assertCount(0, $fields);

        //Creating fields
        factory(RequestField::class)->create();
        factory(RequestField::class)->create([
            'req_type_id' => $type->id
        ]);
        factory(RequestField::class)->create([
            'req_type_id' => $type->id
        ]);

        $fields = $this->repo->getByTypeID($type->id);

        $this->assertTrue($fields instanceof  \Illuminate\Database\Eloquent\Collection);
        $this->assertCount(2, $fields);
    }

}