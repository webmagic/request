<?php

use Tests\TestCase;
use Tests\Unit\Request\RequestRepo;
use Webmagic\Request\Request\Request;
use Webmagic\Request\RequestItem\RequestItem;
use Webmagic\Request\RequestType\RequestType;

class RequestRepoTest extends TestCase
{

    /** @var  RequestRepo */
    protected $repo;

    public function setUp()
    {
        parent::setUp();

        $this->repo = new RequestRepo();
        $this->repo->setEntity(Request::class);
    }



    public function testGetByTypeId()
    {
        $type = factory(RequestType::class)->create();

        //Check instance of empty collection
        $fields = $this->repo->getByTypeID($type->id);
        $this->assertTrue($fields instanceof \Illuminate\Database\Eloquent\Collection);
        $this->assertCount(0, $fields);

        //Creating requests
        factory(Request::class)->create();
        factory(Request::class)->create([
            'req_type_id' => $type->id
        ]);
        factory(Request::class)->create([
            'req_type_id' => $type->id
        ]);

        $fields = $this->repo->getByTypeID($type->id);

        $this->assertTrue($fields instanceof  \Illuminate\Database\Eloquent\Collection);
        $this->assertCount(2, $fields);
    }


    public function testDestroyByTypeID()
    {
        $type = factory(RequestType::class)->create();

        //Creating requests
        factory(Request::class)->create();
        factory(Request::class)->create([
            'req_type_id' => $type->id
        ]);
        factory(Request::class)->create([
            'req_type_id' => $type->id
        ]);

        $first_request = factory(Request::class)->create();
        $second_request = factory(Request::class)->create([
            'req_type_id' => $type->id
        ]);
        $third_request = factory(Request::class)->create([
            'req_type_id' => $type->id
        ]);


        $this->assertDatabaseHas('req_requests', [
            'id' => $first_request->id,
        ]);
        $this->assertDatabaseHas('req_requests', [
            'id' => $second_request->id,
        ]);
        $this->assertDatabaseHas('req_requests', [
            'id' => $third_request->id,
        ]);

        $this->repo->destroyByTypeID($type->id);

        $this->assertDatabaseHas('req_requests', [
            'id' => $first_request->id,
        ]);
        $this->assertDatabaseMissing('req_requests', [
            'id' => $second_request->id,
        ]);
        $this->assertDatabaseMissing('req_requests', [
            'id' => $third_request->id,
        ]);
    }



    public function testDestroy()
    {
        $first_request = factory(Request::class)->create();
        $second_request = factory(Request::class)->create();

        $first_item = factory(RequestItem::class)->create([
            'request_id' => $first_request->id
        ]);

        //Check isset in db
        $this->assertDatabaseHas('req_requests', [
            'id' => $first_request->id,
        ]);
        $this->assertDatabaseHas('req_items', [
            'id' => $first_item->id,
        ]);


        //Destroying
        $this->repo->destroy($first_request->id);

        $first_destroyed_entity = Request::find($first_request->id);

        //Check not found entities
        $this->assertNull($first_destroyed_entity);
        $this->assertDatabaseMissing('req_requests', [
            'id' => $first_request->id,
        ]);
        $this->assertDatabaseMissing('req_items', [
            'id' => $first_item->id,
        ]);
        //Check don't remove superfluous
        $this->assertDatabaseHas('req_requests', [
            'id' => $second_request->id,
        ]);
    }
}