<?php

namespace Tests\Unit\Request;

use Webmagic\Request\Request\RequestRepo as BaseRepo;

class RequestRepo extends BaseRepo
{
    public function setEntity($entity_name)
    {
        $this->entity = $entity_name;
    }
}