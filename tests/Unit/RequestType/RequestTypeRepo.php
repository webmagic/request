<?php

namespace Tests\Unit\RequestType;

use Webmagic\Request\RequestType\RequestTypeRepo as BaseRepo;

class RequestTypeRepo extends BaseRepo
{
    public function setEntity($entity_name)
    {
        $this->entity = $entity_name;
    }
}