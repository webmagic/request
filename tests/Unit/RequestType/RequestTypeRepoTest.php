<?php


use Tests\TestCase;
use Tests\Unit\RequestType\RequestTypeRepo;
use Webmagic\Request\Request\Request;
use Webmagic\Request\RequestField\RequestField;
use Webmagic\Request\RequestType\RequestType;
use IvanLemeshev\Laravel5CyrillicSlug\Slug;

class RequestTypeRepoTest extends TestCase
{

    /** @var  RequestTypeRepo */
    protected $repo;

    public function setUp()
    {
        parent::setUp();

        $this->repo = new RequestTypeRepo();
        $this->repo->setEntity(RequestType::class);
    }



    public function testGetById()
    {
        $type_name = 'request type';
        $new_type = factory(RequestType::class)->create([
            'name' => $type_name
        ]);

        factory(RequestField::class, 2)->create([
            'req_type_id' => $new_type->id
        ]);

        //If id wrong
        $test_id = 0;
        $type = $this->repo->getByID($test_id);
        $this->assertEquals($type, null);

        //Without fields
        $type = $this->repo->getByID($new_type->id, false);

        $this->assertTrue(is_subclass_of($type, \Illuminate\Database\Eloquent\Model::class));
        $this->assertEquals($type_name, $type->name);
        $this->assertArrayNotHasKey('fields', $type->toArray());

        //With fields
        $type = $this->repo->getByID($new_type->id);

        $this->assertTrue(is_subclass_of($type, \Illuminate\Database\Eloquent\Model::class));
        $this->assertEquals($type_name, $type->name);
        $this->assertArrayHasKey('fields', $type);
        $this->assertEquals(2, count($type->fields));
    }


    public function testGetBySlug()
    {
        $slug = new Slug();

        $type_name = 'request type';
        $type_slug = $slug->make($type_name, '-');

        factory(RequestType::class)->create([
            'name' => $type_name,
            'slug' => $type_slug
        ]);

        //If slug is empty
        $test_slug = '';
        $type = $this->repo->getBySlug($test_slug);
        $this->assertEquals($type, null);

        //if slug is false
        $test_slug = 'test';
        $type = $this->repo->getBySlug($test_slug);
        $this->assertEquals($type, null);

        //if slug is true
        $type = $this->repo->getBySlug($type_slug);
        $this->assertTrue(is_subclass_of($type, \Illuminate\Database\Eloquent\Model::class));
        $this->assertEquals($type_name, $type->name);
    }


    public function testGetAll()
    {
        $types = $this->repo->getAll();

        $this->assertTrue($types instanceof \Illuminate\Database\Eloquent\Collection);
        $this->assertCount(0, $types);

        $first_type = factory(RequestType::class)->create();
        $second_type = factory(RequestType::class)->create();

        $request_count = 3;
        factory(Request::class, $request_count)->create([
            'req_type_id' => $first_type->id
        ]);


        //Without requests
        $types = $this->repo->getAll();

        $this->assertTrue($types instanceof \Illuminate\Database\Eloquent\Collection);
        $this->assertCount(2, $types);
        $this->assertArrayNotHasKey('requests', $types->first()->toArray());

        //With products
        $types = $this->repo->getAll(true);

        $this->assertTrue($types instanceof \Illuminate\Database\Eloquent\Collection);
        $this->assertArrayHasKey('requests', $types->first()->toArray());
        $this->assertEquals(3, count($types->first()['requests']));
        $this->assertEquals(0, count($types->last()['requests']));
    }



    public function testDestroy()
    {
        $first_type = factory(RequestType::class)->create();
        $second_type = factory(RequestType::class)->create();

        $first_field = factory(RequestField::class)->create([
            'req_type_id' => $first_type->id
        ]);

        //Check isset in db
        $this->assertDatabaseHas('req_types', [
            'id' => $first_type->id,
        ]);
        $this->assertDatabaseHas('req_fields', [
            'id' => $first_field->id,
        ]);

        //Destroying
        $this->repo->destroy($first_type->id);

        $first_destroyed_entity = RequestType::find($first_type->id);

        //Check not found entities
        $this->assertNull($first_destroyed_entity);
        $this->assertDatabaseMissing('req_types', [
            'id' => $first_type->id,
        ]);
        $this->assertDatabaseMissing('req_fields', [
            'id' => $first_field->id,
        ]);
        //Check don't remove superfluous
        $this->assertDatabaseHas('req_types', [
            'id' => $second_type->id,
        ]);
    }

}