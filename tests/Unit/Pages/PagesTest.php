<?php

namespace Tests\Unit\Pages;

use Tests\TestCase;
use Webmagic\Request\RequestField\RequestField;
use Webmagic\Request\RequestType\RequestType;
use Webmagic\Users\Models\User;

class PagesTest extends TestCase
{
    /**
     * RequestType
     */
    //Page for types list
    public function testRequestTypeIndex()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('request_type::index'));
        $response->assertStatus(200);
    }

    //Page for type creating
    public function testRequestTypeCreate()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('request_type::create'));
        $response->assertStatus(200);
    }

    //Page for type editing
    public function testRequestTypeEdit()
    {
        $user = new User();
        $type = factory(RequestType::class)->create();

        $response = $this->actingAs($user)->get(route('request_type::edit', $type->id));
        $response->assertStatus(200);
    }



    /**
     * RequestField
     */
    //Page for field creating
    public function testRequestFieldCreate()
    {
        $user = new User();
        $type = factory(RequestType::class)->create();

        $response = $this->actingAs($user)->get(route('request_field::create', $type->id));
        $response->assertStatus(200);
    }

    //Page for field editing
    public function testRequestFieldEdit()
    {
        $user = new User();
        $field = factory(RequestField::class)->create();

        $response = $this->actingAs($user)->get(route('request_field::edit', $field->id));
        $response->assertStatus(200);
    }



    /**
     * Request
     */
    //Page for requests list for type and counts
    public function testRequestIndex()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('request::index'));
        $response->assertStatus(200);
    }


    //Page for requests info  by type
    public function testRequestShow()
    {
        $user = new User();
        $type = factory(RequestType::class)->create();

        $response = $this->actingAs($user)->get(route('request::show', $type->id));
        $response->assertStatus(200);
    }
}
