<h1>{{$type['name']}}</h1>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h4>@lang('request::common.request-creation-url'): {{$type->present()->requestURL}} <a
                            href="{{route('request::export', $type['id'])}}"
                            class="pull-right btn btn-sm btn-success pull-right products-btn"
                            title="@lang('request::common.export-to-excel')"><i
                                class="fa fa-file-excel-o"></i> @lang('request::common.export')</a></h4>
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active" data-original-title="" title="">
                        <a href="#tab_1" data-toggle="tab" aria-expanded="true" data-original-title=""
                           title="">Requests</a>
                    </li>
                    <li data-original-title="" title="">
                        <a href="#tab_2" data-toggle="tab" aria-expanded="true" data-original-title="" title="">Spam</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="box-body tab-pane  active  table-responsive no-padding" id="tab_1">
                        @if(count($requests))
                            <table class="table table-hover dataTable">
                                <thead>
                                <tr>
                                    <th style="min-width: 40px;">@lang('request::common.id')</th>
                                    @foreach($column_names as $name)
                                        <th>{{$name}}</th>
                                    @endforeach
                                    <th>@lang('request::common.date')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    @if ($request[$spam_field])
                                        @continue
                                    @endif

                                    <tr class="even products-row js_request_{{$request->id}}">
                                        <td>{{$request->id}}</td>

                                        @foreach($column_names as $id => $name)
                                            @if($item =  $request->items->where('req_field_id', $id)->first())
                                                <td>
                                                    @if(isset($item['field']['type'] ) && $item['field']['type'] === 'file' && isset($item->value))
                                                        <a href="{{$item->present()->prepareFileURL}}" target="_blank">
                                                            File
                                                        </a>
                                                    @else
                                                        {{$item->value ?? ''}}
                                                    @endif
                                                </td>
                                            @else
                                                <td></td>
                                            @endif
                                        @endforeach

                                        <td width="100px">
                                            {{$request->created_at}}
                                        </td>
                                        <td width="100px">
                                            <div class="btn-group">
                                                <button type="button" data-item=".js_request_{{$request->id}}"
                                                        data-method="DELETE"
                                                        data-request="{{route('request::delete', $request->id)}}"
                                                        class="js_delete personals-delete btn btn-danger btn-flat"
                                                        title="@lang('request::common.delete')"><i
                                                            class="fa fa-trash"></i>
                                                </button>
                                                <form style="display: inline-block; margin-left: 10px;" method="post" id="myform" class="js-submit" action="{{route('request::edit', $request->id)}}">
                                                    <input type="hidden" name="spam" value="1">
                                                    <button class="btn btn-warning" type="submit" data-item=".js_request_{{$request->id}}"
                                                            title="Mark as spam">
                                                        <i class="fa fa-ban"></i>
                                                        <i></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="text-center">
                                @lang('request::common.nothing-found')
                            </p>
                        @endif
                    </div>
                    <div class="box-body tab-pane  table-responsive no-padding" id="tab_2">
                        @if(count($requests))
                            <table class="table table-hover dataTable">
                                <thead>
                                <tr>
                                    <th style="min-width: 40px;">@lang('request::common.id')</th>
                                    @foreach($column_names as $name)
                                        <th>{{$name}}</th>
                                    @endforeach
                                    <th>@lang('request::common.date')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)

                                    @if (!$request[$spam_field])
                                        @continue
                                    @endif

                                    <tr class="even products-row js_request_{{$request->id}}">
                                        <td>{{$request->id}}</td>
                                        @foreach($request->items as $item)
                                            <td>{{$item->value ?? ''}}</td>
                                        @endforeach
                                        <td width="100px">
                                            {{$request->created_at}}
                                        </td>
                                        <td width="100px">
                                            <div class="btn-group">
                                                <button type="button" data-item=".js_request_{{$request->id}}"
                                                        data-method="DELETE"
                                                        data-request="{{route('request::delete', $request->id)}}"
                                                        class="js_delete personals-delete btn btn-danger btn-flat"
                                                        title="@lang('request::common.delete')"><i
                                                            class="fa fa-trash"></i>
                                                </button>
                                                <form style="display: inline-block; margin-left: 10px;" method="post" id="myform" class="js-submit" action="{{route('request::edit', $request->id)}}">
                                                    <input type="hidden" name="spam" value="0">
                                                    <button class="btn btn-info" type="submit" data-item=".js_request_{{$request->id}}"
                                                            title="Remove from spam">
                                                        <i class="fa fa-thumbs-up"></i>
                                                        <i></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="text-center">
                                @lang('request::common.nothing-found')
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
