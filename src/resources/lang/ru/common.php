<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Common translation for request
    |--------------------------------------------------------------------------
    |
    */
    'requests' => 'Заявки',
    'name' => 'Имя',
    'requests-count' => 'Количество заявок',
    'export' => 'Экспортировать',
    'export-to-excel' => 'Экспортировать в Excel',
    'configurations' => 'Настройки',
    'all-requests' => 'Все заявки',
    'nothing-found' => 'Ничего не найдено',
    'delete-all-requests-of-this-type' => 'Удалить заявки этого типа',
    'all-requests-types' => 'Все типы заявок',
    'add' => 'Добавить',
    'id' => 'ID',
    'slug' => 'Slug',
    'delete' => 'Удалить',
    'active' => 'Активен',
    'unactive' => 'Не активен',
    'request-type-creation' => 'Создание нового типа заявок',
    'request-type-edition' => 'Редактирование типа заявок',
    'status' => 'Статус',
    'description' => 'Описание',
    'event-select' => 'Событие, которое будет сгенериовано при получении запроса',
    'save' => 'Сохранить',
    'slug-input' => 'Slug (Только латиница. Используеться для генерации роута на прием заявок)',
    'date' => 'Дата',
    'request-creation-url' => 'Роут для регистрации заявок',
    'request-available-fields' => 'Доступные поля заявки',
    'type' => 'Тип',
    'validation-rules' => 'Правила валидации',
    'field-creation' => 'Создание нового поля',
    'field-edition' => 'Редактирвоание поля',
    'available-validation-rules' => 'Доступные правила валидации можно посмотреть здесь',


];