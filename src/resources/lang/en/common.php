<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Common translation for request
    |--------------------------------------------------------------------------
    |
    */
    'requests' => 'Requests',
    'name' => 'Name',
    'requests-count' => 'Requests count',
    'export' => 'Export',
    'export-to-excel' => 'Export to Excel',
    'configurations' => 'Settings',
    'all-requests' => 'All requests',
    'nothing-found' => 'Nothing found',
    'delete-all-requests-of-this-type' => 'Delete all requests of this type',
    'all-requests-types' => 'All requests types',
    'add' => 'Add',
    'id' => 'ID',
    'slug' => 'Slug',
    'delete' => 'Delete',
    'active' => 'Active',
    'unactive' => 'Unactive',
    'request-type-creation' => 'Request type creation',
    'request-type-edition' => 'Request type edition',
    'status' => 'Status',
    'description' => 'Description',
    'event-select' => 'Event which will fired when request created',
    'save' => 'Save',
    'slug-input' => 'Slug (Latin characters only. Uses for an API url generation)',
    'date' => 'Data',
    'request-creation-url' => 'URL of request registration API',
    'request-available-fields' => 'Available fields',
    'type' => 'Type',
    'validation-rules' => 'Validation rules',
    'field-creation' => 'Field creation',
    'field-edition' => 'Field edition',
    'available-validation-rules' => 'Available validation rules',
];