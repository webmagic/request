<?php

namespace Webmagic\Request;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Illuminate\Support\Facades\Event;
use Webmagic\Request\Request\RequestRepoContract;
use Webmagic\Request\RequestField\RequestFieldRepoContract;
use Webmagic\Request\RequestItem\RequestItemRepoContract;
use Webmagic\Request\RequestType\RequestTypeRepoContract;
use IvanLemeshev\Laravel5CyrillicSlug\Slug;
use Webmagic\Request\PHPDocs\PHPDocGenerationTrait;
use Illuminate\Support\Facades\Storage;




/**********************************************************************************************************************
 * Webmagic\Request\RequestType\RequestTypeRepoContract
 **********************************************************************************************************************
 *
 * @method void typeGetBySlug(string $slug, bool $active_only)
 * @method void typeDestroy(string $entity_id)
 * @method void typeGetByID(string $entity_id)
 * @method void typeGetAll($perPage = null)
 * @method void typeGetAllActive()
 * @method void typeCreate(array $entity_data)
 * @method void typeUpdate(string $entity_id, array $entity_data)
 * @method void typeDestroyAll()
 * @method array typeGetForSelect(string $value, string $key)
 *
 **********************************************************************************************************************

 **********************************************************************************************************************
 * Webmagic\Request\RequestField\RequestFieldRepoContract
 **********************************************************************************************************************
 *
 * @method void fieldGetByTypeID(string $type_id)
 * @method void fieldGetForSelectByTypeID(string $type_id, string $value, string $key)
 * @method void fieldGetAll()
 * @method void fieldGetAllActive()
 * @method void fieldGetByID(string $entity_id)
 * @method void fieldCreate(array $entity_data)
 * @method void fieldUpdate(string $entity_id, array $entity_data)
 * @method void fieldDestroy(string $entity_id)
 * @method void fieldDestroyAll()
 * @method array fieldGetForSelect(string $value, string $key)
 *
 **********************************************************************************************************************

 **********************************************************************************************************************
 * Webmagic\Request\Request\RequestRepoContract
 **********************************************************************************************************************
 *
 * @method void requestGetByTypeID(string $type_id, bool $with_items)
 * @method void requestDestroyByTypeID(string $type_id)
 * @method void requestDestroy(string $request_id)
 * @method void requestGetAll()
 * @method void requestGetAllActive()
 * @method void requestGetByID(string $entity_id)
 * @method void requestCreate(array $entity_data)
 * @method void requestUpdate(string $entity_id, array $entity_data)
 * @method void requestDestroyAll()
 * @method array requestGetForSelect(string $value, string $key)
 *
 **********************************************************************************************************************

 **********************************************************************************************************************
 * Webmagic\Request\RequestItem\RequestItemRepoContract
 **********************************************************************************************************************
 *
 * @method void itemGetAll()
 * @method void itemGetAllActive()
 * @method void itemGetByID(string $entity_id)
 * @method void itemCreate(array $entity_data)
 * @method void itemUpdate(string $entity_id, array $entity_data)
 * @method void itemDestroy(string $entity_id)
 * @method void itemDestroyAll()
 * @method array itemGetForSelect(string $value, string $key)
 *
 *********************************************************************************************************************/


class RequestService
{

    use PHPDocGenerationTrait;

    /** @var array Abstract entities for which access provided  */
    protected $classes = [
        'type' => RequestTypeRepoContract::class,
        'field' => RequestFieldRepoContract::class,
        'request' => RequestRepoContract::class,
        'item' => RequestItemRepoContract::class,
    ];

    /**
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        foreach ($this->classes as $alias => $abstract){
            if(strpos($method, $alias) === 0){
                $obj = app()->make($this->classes[$alias]);
                return $this->callMethod($obj, $args, $method, $alias);
            }
        }

        return new MethodNotAllowedException($method);
    }

    /**
     * Call method on obj with method name cleaning
     *
     * @param $obj
     * @param $args
     * @param $fullMethodName
     * @param $removeFromMethod
     *
     * @return mixed
     */
    protected function callMethod($obj, $args, $fullMethodName, $removeFromMethod)
    {
        $method = str_replace($removeFromMethod, '', $fullMethodName);

        try{
            return call_user_func_array([$obj, $method], $args);
        } catch (MethodNotAllowedException $e){
            return $e;
        }
    }



    /**
     * Generate slug by name
     *
     * @param string $name
     * @return string
     */
    public function slugGenerate(string $name)
    {
        $slug = new Slug();
        return $slug->make($name, '-');
    }



    /**
     * Export requests to Excel
     *
     * @param $type
     * @param $requests
     * @param $fields
     */
    public function exportToExcel($type, $requests, $fields)
    {
        //Generate array for export
        foreach ($requests as $request)
        {
            $items = $request['items']->keyBy('req_field_id')->toArray();
            $tmp_request = [];
            foreach($fields as $field_id => $field_name)
            {
                $tmp_request[$field_name] = isset($items[$field_id]) ? $items[$field_id]['value'] : '';
            }
            //add registration date of request
            $tmp_request['date'] = $request['created_at'];
            $requests_for_export[] = $tmp_request;
        }

        if(!isset($requests_for_export))
            $requests_for_export = 0;

        Excel::create($type['slug'], function($excel) use ($requests_for_export){
            $excel->sheet('Экспорт заявок', function($sheet) use ($requests_for_export){
                $sheet->fromArray($requests_for_export);
            });
        })->download('xls');
    }

    /**
     * Find all events from config
     *
     * @return mixed
     */
    public function findEvents()
    {
        $events  = config('webmagic.request.events');

        foreach($events as $event)
        {
            $names[$event] = $event;
        }
        return $names;
    }


    /**
     * Generate event by request name
     *
     * @param $type_id
     * @param $request_data
     */
    public function fireEvent($type_id, $request_data)
    {
       $type = $this->typeGetByID($type_id);

        Event::fire(new $type['event'](['data' => $request_data]));
    }


    /**
     * Return rules for fields for request by type id
     *
     * @param $type_id
     * @return mixed
     */
    public function getRulesForRequestByType($type_id)
    {
        return $this->fieldGetForSelectByTypeID($type_id, 'rules', 'name');
    }


    /**
     * Saving data from fields from request
     *
     * @param $request_id
     * @param $type_id
     * @param $request_data
     */
    public function saveItems($request_id, $type_id, $request_data)
    {
        //Creating items
        $request_fields = $this->fieldGetForSelectByTypeID($type_id, 'id', 'name');

        foreach ($request_data as $field_name => $value)
        {

            // check if field empty skip it
            if (empty($request_fields[$field_name]) || !$value) {
                continue;
            }

            $field_id = $request_fields[$field_name];

            // check if array (for example it can be array of checkboxes) and
            // transform it to string
            if(is_array($value)){
                $value = implode( ', ', $value);
            }

            //check on file
            if(is_file($value)){
                $value = $this->saveFileAndGetName($value, $request_id);
            }

            $this->itemCreate([
                'request_id' => $request_id,
                'req_field_id' => $field_id,
                'value' => $value
            ]);

        }

    }


    /**
     * Saving file and get name of file
     *
     * @param UploadedFile $file
     * @param int $request_id
     *
     * @return string
     */
    protected function saveFileAndGetName(UploadedFile $file, int $request_id)
    {
        $basePath = config('webmagic.request.files_directory');
        $filesDirectory = Storage::disk(config('webmagic.request.storage_disk_name'))->path("$basePath/$request_id");

        $name = $file->getClientOriginalName();
        $file->move($filesDirectory, $name);

        return ("$request_id/$name");
    }



    /**
     * Generate collections of requests, if item empty put empty array in item collection
     *
     * @param Collection $requests_with_fields
     * @param Collection $fields
     * @return Collection
     */
    public function generateRequestsCollectionWithFields(Collection $requests_with_fields, Collection $fields): Collection
    {
        foreach($requests_with_fields as $request)
        {
            $items = $request['items']->keyBy('req_field_id')->toArray();
            foreach ($fields as $field)
            {
                if(!isset($items[$field['id']])){
                    $request['items'][] = [$field['id'] => []];
                }
            }
        }

        return $requests_with_fields;
    }
}
