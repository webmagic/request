<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('req_requests', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('req_type_id')->unsigned()->nullable();
            $table->foreign('req_type_id')->references('id')->on('req_types')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('req_requests');
    }
}
