<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReqItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('req_items', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('value');

            $table->integer('request_id')->unsigned()->nullable();
            $table->foreign('request_id')->references('id')->on('req_requests')->onDelete('cascade');

            $table->integer('req_field_id')->unsigned()->nullable();
            $table->foreign('req_field_id')->references('id')->on('req_fields')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('req_items');
    }
}
