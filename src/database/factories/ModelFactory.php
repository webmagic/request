<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/**
 * Types
 */
$factory->define(\Webmagic\Request\RequestType\RequestType::class, function (Faker\Generator $faker) {
    return [
        'name' => $name = $faker->word,
        'slug' => $name,
        'active' => true,
        'description' => $faker->text(),
    ];
});

/**
 * Fields
 */
$factory->define(\Webmagic\Request\RequestField\RequestField::class, function (Faker\Generator $faker) {
    return [
        'name' => $name = $faker->word,
        'type' => 'string',
        'rules' => 'required',
    ];
});


/**
 * Requests
 */
$factory->define(\Webmagic\Request\Request\Request::class, function ($faker) {
    return [
        'req_type_id' => function () {
            return factory(\Webmagic\Request\RequestType\RequestType::class)->create()->id;
        }
    ];
});


/**
 * Items
 */
$factory->define(\Webmagic\Request\RequestItem\RequestItem::class, function (Faker\Generator $faker) {
    return [
        'value' => $faker->word,
    ];
});