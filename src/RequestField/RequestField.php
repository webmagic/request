<?php

namespace Webmagic\Request\RequestField;


use Illuminate\Database\Eloquent\Model;

class RequestField extends Model
{

    /** @var string  */
    protected $table = 'req_fields';

    /** @var array  */
    protected $fillable = ['name', 'type', 'rules', 'req_type_id'];


    /**
     * Request fields constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, config('webmagic.request.req_fields_available_fields'));

        parent::__construct($attributes);
    }


    /** @var array  */
    protected $validation_rules = [
        'name' => 'required|string',
        'type' => 'required'
    ];


    /**
     * Return rules for validation
     *
     * @return array
     */
    public function getRules()
    {
        return $this->validation_rules;
    }


    /**
     * Relations with type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function types()
    {
        return $this->belongsTo('Webmagic\Request\RequestType\RequestType');
    }

    /**
     * Prevent set null
     *
     * @param $value
     *
     * @return string
     */
    public function setRulesAttribute($value)
    {
        $this->attributes['rules'] =  (bool)$value ? $value : '';
    }
}