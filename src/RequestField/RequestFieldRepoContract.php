<?php


namespace Webmagic\Request\RequestField;


use Webmagic\Core\Entity\EntityRepoInterface;

interface RequestFieldRepoContract extends EntityRepoInterface
{

    /**
     * Return field by type id
     *
     * @param $type_id
     * @return mixed
     */
    public function getByTypeID($type_id);


    /**
     * Get from DB by type Id and prepare array with ID as key and name as value
     *
     * @param $type_id
     * @param string $value
     * @param null $key
     * @return array|\Illuminate\Support\Collection
     */
    public function getForSelectByTypeID($type_id, $value = 'id', $key = null);
}