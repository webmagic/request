<?php

namespace Webmagic\Request\RequestField;

use Webmagic\Core\Entity\EntityRepo;

class RequestFieldRepo extends EntityRepo implements RequestFieldRepoContract
{
    /** Entiry for manipulation */
    protected $entity = RequestField::class;

    
    /**
     * Return field by type id
     * 
     * @param $type_id
     * @return mixed
     */
    public function getByTypeID($type_id)
    {
        $query = $this->query();
        $query->where('req_type_id', $type_id);

        return $this->realGetMany($query);
    }



    /**
     * Get from DB by type Id and prepare array with ID as key and name as value
     *
     * @param $type_id
     * @param string $value
     * @param null $key
     * @return array|\Illuminate\Support\Collection
     */
    public function getForSelectByTypeID($type_id, $value = 'id', $key = null)
    {
        $query = $this->query();
        $query->where('req_type_id', $type_id);

        if (!$entities = $query->pluck($value, $key)) {
            return $entities;
        }

        return $entities->toArray();
    }
}