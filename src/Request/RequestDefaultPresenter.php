<?php


namespace Webmagic\Request\Request;


use Carbon\Carbon;
use Webmagic\Core\Presenter\Presenter;

class RequestDefaultPresenter extends Presenter
{
    /**
     * Present request as array
     *
     * @return array
     */
    public function asArray(): array
    {
        $data = [
            'id' => $this->entity->id,
            'created_at' => Carbon::parse($this->entity->created_at)->format('d.m.Y H:i'),
            'date' => Carbon::parse($this->entity->created_at)->format('d.m.Y'),
            'time' => Carbon::parse($this->entity->created_at)->format('H:i'),
            'type' => $this->entity->type->slug,
            'name' => $this->entity->type->name,
            'admin_panel_link' => $this->linkInAdminPanel(),
            'request' => $this->prepareFields()
        ];

        return $data;
    }

    /**
     * Prepare link for all request in admin panel
     *
     * @return string
     */
    public function linkInAdminPanel()
    {
        return route('request::index');
    }

    /**
     * Prepare data for fields
     *
     * @return array
     */
    protected function prepareFields()
    {
        $fieldsData = [];
        foreach ($this->entity->items as $item) {
            $fieldsData[$item->field->name] = $item->value;
        }

        return $fieldsData;
    }
}