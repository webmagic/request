<?php

namespace Webmagic\Request\Request;


use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Presenter\PresentableTrait;
use Webmagic\Request\RequestField\RequestField;
use Webmagic\Request\RequestType\RequestType;

class Request extends Model
{
    use PresentableTrait;

    /** @var string  */
    protected $presenter = RequestDefaultPresenter::class;

    /** @var   */
    protected $table = 'req_requests';

    /** @var   */
    protected $fillable = ['req_type_id', 'spam'];


    /** @var bool  */
    public $timestamps = true;


    /**
     * Request constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, config('webmagic.request.request_available_fields'));

        parent::__construct($attributes);
    }

    /**
     * Relations with items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('Webmagic\Request\RequestItem\RequestItem');
    }

    /**
     * Relation with Request Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(RequestType::class, 'req_type_id');
    }

    /**
     * Relation to fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function fields()
    {
        return $this->hasManyThrough(RequestField::class, RequestType::class, 'id', 'req_type_id', 'req_type_id');
    }
}