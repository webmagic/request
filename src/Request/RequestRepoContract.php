<?php


namespace Webmagic\Request\Request;


use Webmagic\Core\Entity\EntityRepoInterface;

interface RequestRepoContract extends EntityRepoInterface
{

    /**
     * Return requests by type id
     *
     * @param $type_id
     * @param bool $with_items
     * @return mixed
     */
    public function getByTypeID($type_id, bool $with_items = false);


    /**
     * Destroy all request by type
     *
     * @param $type_id
     * @return mixed
     */
    public function destroyByTypeID($type_id);


    /**
     * Destroy request with items
     *
     * @param int $request_id
     *
     * @return int
     */
    public function destroy($request_id);
}