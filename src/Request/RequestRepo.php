<?php

namespace Webmagic\Request\Request;

use Illuminate\Database\Eloquent\Builder;
use Webmagic\Core\Entity\EntityRepo;

class RequestRepo extends EntityRepo implements RequestRepoContract
{

    /** Entiry for manipulation */
    protected $entity = Request::class;


    /**
     * Return requests by type id
     *
     * @param      $type_id
     * @param bool $with_items
     *
     * @return mixed
     * @throws \Exception
     */
    public function getByTypeID($type_id, bool $with_items = false)
    {
        $query = $this->query();

        $query->where('req_type_id', $type_id);


        if($with_items){
            $query->with('items');
        }

        $query->orderBy('created_at', 'desc');

        return $this->realGetMany($query);
    }


    /**
     * Destroy all request by type
     *
     * @param $type_id
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroyByTypeID($type_id)
    {
        $query = $this->query();
        $requests = $query->where('req_type_id', $type_id);

        foreach($requests as $request)
        {
            $request->items()->delete();
        }

        return $requests->delete();
    }


    /**
     * Destroy request with items
     *
     * @param int $request_id
     *
     * @return int
     */
    public function destroy($request_id)
    {
        $query = $this->query();
        $request = $query->find($request_id);

        $request->items()->delete();

        return  $request::destroy($request_id);
    }

    /**
     * @param Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator
     */
    protected function realGetMany(Builder $query, $per_page = null)
    {
        $query = $this->addAllRelations($query);

        return parent::realGetMany($query, $per_page);
    }

    /**
     * @param Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    protected function realGetOne(Builder $query)
    {
        $query = $this->addAllRelations($query);

        return parent::realGetOne($query);
    }

    /**
     * Add all relations to query
     *
     * @param Builder $query
     *
     * @return Builder
     */
    protected function addAllRelations(Builder $query)
    {
        return $query->with(['type', 'items' => function($query){
            $query->with('field');
        }]);
    }

}