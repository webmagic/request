<?php

namespace Webmagic\Request;

use Illuminate\Routing\Router;
use Webmagic\Dashboard\Components\Menus\MainMenu\Item;
use Webmagic\Dashboard\Dashboard;


class RequestDashboardServiceProvider extends RequestServiceProvider
{

    /**
     * Register Request services
     */
    public function register()
    {
        parent::register();

        $this->mergeConfigFrom(
            __DIR__.'/config/request_dashboard.php', 'webmagic.dashboard.request'
        );
    }

    /**
     * Register routes
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        //Load Routs
        $this->loadRoutesFrom(__DIR__ . '/routes/routes_dashboard.php');

        //Config publishing
        $this->publishes([
            __DIR__.'/config/request_dashboard.php' => config_path('webmagic/dashboard/request.php'),
        ], 'config');

        //Views publishing
        $this->publishes([
            __DIR__.'/resources/views/' => base_path('resources/views/vendor/request/'),
        ], 'views');

        $this->loadTranslationsFrom(__DIR__.'/resources/lang/', 'request');

        //Add dashboard menu item
        $this->includeMenuForDashboard();
    }

    /**
     * Including menu items for new dashboard
     */
    protected function includeMenuForDashboard()
    {
        $menu_item_config = config('webmagic.dashboard.request.dashboard_menu_item');

        if(!$menu_item_config) {
            return;
        }

        //Add menu into global menu
        $dashboard = app()->make(Dashboard::class);
        $dashboard->getMainMenu()->addMenuItems($menu_item_config);
    }

}