<?php

namespace Webmagic\Request\RequestType;


use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Presenter\PresentableTrait;


/**
 * Class RequestType
 * @package Webmagic\Request\RequestType
 *
 * @method RequestTypePresenter present()
 */
class RequestType extends Model
{
    use PresentableTrait;

    /** @var string  */
    protected $presenter = RequestTypePresenter::class;

    /** @var string  */
    protected $table = 'req_types';

    /** @var array  */
    protected $fillable = ['slug', 'name', 'description', 'event', 'active'];


    /**
     * Request types constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, config('webmagic.request.req_types_available_fields'));

        parent::__construct($attributes);
    }


    /**
     * Relations with fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fields()
    {
        return $this->hasMany('Webmagic\Request\RequestField\RequestField', 'req_type_id');
    }


    /**
     * Relations with requests
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function requests()
    {
        return $this->hasMany('Webmagic\Request\Request\Request', 'req_type_id');
    }



}