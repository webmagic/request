<?php


namespace Webmagic\Request\RequestType;


use Illuminate\Database\Eloquent\Collection;
use Webmagic\Core\Entity\EntityRepoInterface;

interface RequestTypeRepoContract extends EntityRepoInterface
{

    /**
     * Return information about type by slug
     *
     * @param string $slug
     * @param bool $active_only
     * @return mixed
     */
    public function getBySlug(string $slug, bool $active_only = false);


    /**
     * Destroy type with fields
     *
     * @param int $entity_id
     *
     * @return int
     */
    public function destroy($entity_id);
}
