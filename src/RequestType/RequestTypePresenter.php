<?php


namespace Webmagic\Request\RequestType;


use Webmagic\Core\Presenter\Presenter;

class RequestTypePresenter extends Presenter
{

    /**
     * Prepare url for creating request
     *
     * @return string
     */
    public function requestURL()
    {
        return route('request-api::create', ['type' => $this->entity->slug]);
    }
}