<?php

namespace Webmagic\Request\RequestItem;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Webmagic\Request\RequestField\RequestField;

class RequestItem extends Model
{
    use PresentableTrait;

    /** @var */
    protected $table = 'req_items';

    /** @var */
    protected $fillable = ['value', 'request_id', 'req_field_id'];

    /** @var bool */
    public $timestamps = true;

    protected $presenter = RequestItemPresenter::class;

    /**
     * Request item constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, config('webmagic.request.req_item_available_fields'));

        parent::__construct($attributes);
    }

    /**
     * Relation to field
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function field()
    {
        return $this->belongsTo(RequestField::class, 'req_field_id');
    }
}
