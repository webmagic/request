<?php

namespace Webmagic\Request\RequestItem;

use Webmagic\Core\Entity\EntityRepo;

class RequestItemRepo extends EntityRepo implements RequestItemRepoContract
{

    /** Entiry for manipulation */
    protected $entity = RequestItem::class;
}