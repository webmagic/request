<?php

namespace Webmagic\Request\RequestItem;

use Illuminate\Support\Facades\Storage;
use Laracasts\Presenter\Presenter;

class RequestItemPresenter extends Presenter
{

    public function getConfigPath()
    {
        return config('webmagic.request.files_directory');
    }


    /**
     * Prepare url for images and files
     *
     *
     * @return string
     */
    protected function prepareFileURL()
    {
        return Storage::disk(config('webmagic.request.storage_disk_name'))->url($this->getConfigPath()."/".$this->entity->value);
    }
}