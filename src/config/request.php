<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Events list
    |--------------------------------------------------------------------------
    |
    | Registration events which will be available for apply to request
    |
    */
    'events' => [
        Webmagic\Request\Events\BaseEvent::class
    ],

    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Can use middleware for sending form
    |
    */
    'middleware' => ['web'],

    /*
   |--------------------------------------------------------------------------
   | Prefix for api route
   |--------------------------------------------------------------------------
   |
   */
    'prefix' => 'request',


    /*
    |--------------------------------------------------------------------------
    | Types list
    |--------------------------------------------------------------------------
    |
    | Types for fields
    |
    */
    'types' => [
        'string' => 'string',
        'text' => 'text',
        'integer' => 'integer',
        'date' => 'date',
        'file' => 'file'
    ],


    /*
    |--------------------------------------------------------------------------
    | Fields available in model additional to base fields
    |--------------------------------------------------------------------------
    |
    */
    'req_types_available_fields' => [],
    'req_fields_available_fields' => [],
    'request_available_fields' => [],
    'req_item_available_fields' => [],

    /*
    |--------------------------------------------------------------------------
    | Spam fields
    |--------------------------------------------------------------------------
    |
    */

    // enable/disable spam checking
    'spam_check' => false,

    // field in database
    'spam_field' => 'spam',

    // check by this field
    'spam_check_field' => 'description',

    /*
    |--------------------------------------------------------------------------
    | Files
    |--------------------------------------------------------------------------
    |
    */

    'files_directory' => 'webmagic/request/upload',

    'storage_disk_name' => 'public'


];