<?php
/*
   |--------------------------------------------------------------------------
   | Module Request. Dashboard settings
   |--------------------------------------------------------------------------
   |
   | This setting needs only if you want use module Dashboard
   |
   */

return [

    /*
    |--------------------------------------------------------------------------
    | Module routes_dashboard prefix
    |--------------------------------------------------------------------------
    |
    | This prefix use for generation all routes for module
    |
    */

    'prefix' => 'dashboard/requests',


    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Can use middleware for access to module page
    |
    */

    'middleware' => ['request', 'auth'],

    /*
    |--------------------------------------------------------------------------
    | Parent category in dashboard
    |--------------------------------------------------------------------------
    |
    | Use for generation module page in dashboard.
    | Use '' if not need parent category
    |
    */

    'menu_parent_category' => '',

    /*
    |--------------------------------------------------------------------------
    | Dashboard menu item config
    |--------------------------------------------------------------------------
    |
    | Config new item in dashboard menu
    |
    */

    'menu_item_name' => 'requests',


    'types' => [
        'string' => 'string',
        'text' => 'text',
        'integer' => 'integer',
        'date' => 'date',
        'file' => 'file'
    ],


    'dashboard_menu_item' => [
        'text' => 'request::common.requests',
        'icon' => 'fa-envelope',
        'rank' => 90,
        'active_rules' => [
            'routes_parts'=>[
                'request::'
            ]
        ],
        'subitems'=>[
            [
                'text' => 'request::common.all-requests',
                'icon' => 'fa-circle-o',
                'rank' => 100,
                'link' => 'dashboard/requests',
                'active_rules' => [
                    'urls'=>[
                        'dashboard/requests'
                    ],
                    'routes_parts'=>[
                        'request::show'
                    ]
                ]
            ],
            [
                'text' => 'request::common.configurations',
                'icon' => 'fa-circle-o',
                'rank' => 100,
                'link' => 'dashboard/requests/type',
                'active_rules' => [
                    'routes_parts'=>[
                        'request_type::',
                        'request_field::'
                    ]
                ]
            ],
        ],
    ]
];
