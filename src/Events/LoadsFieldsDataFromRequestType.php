<?php


namespace Webmagic\Request\Events;


use Illuminate\Support\Collection;
use Webmagic\Request\RequestService;

trait LoadsFieldsDataFromRequestType
{
    /**
     * Define a request type as a static attribute
     */
//    protected static $request_type = 'request-type;

    /**
     * Fields (names) that can be used in admin panel
     * @return array
     * @throws \Exception
     */
    public static function getAvailableFields(): array
    {

        $default_fields = self::getDefaultFields();

        if(!isset(self::$request_type)){
            throw new \Exception('Attribute $request_type should be defined');
        }

        $requestService = app()->make(RequestService::class);

        /** @var Collection $fields */
        $fields = $requestService->typeGetBySlug(self::$request_type)->fields;


        if(!count($fields)){
            return $default_fields;
        }


        $fields = $fields->keyBy(function($item){
            return "request.$item->name";
        })->keys()->toArray();

        $fields = array_merge($default_fields, $fields);

        return $fields;
    }

    /**
     * Get files
     *
     * @return array
     */
    public static function getAvailableFiles(): array
    {
        $requestService = app()->make(RequestService::class);

        $files = $requestService->typeGetFilesBySlug(self::$request_type)->fields;

        if(!count($files)){
            return [];
        }
        $prepared_files = [];

        foreach ($files as $file){
            $prepared_files[$file['id']] = $file['name'];
        }

        return $prepared_files;
    }

    /**
     * Base fields
     *
     * @return array
     */
    protected static function getDefaultFields(): array
    {
        return [
            'id',
            'created_at',
            'date',
            'time',
            'type',
            'name',
            'admin_panel_link'
        ];
    }
}