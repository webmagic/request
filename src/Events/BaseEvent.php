<?php

namespace Webmagic\Request\Events;

use     Illuminate\Queue\SerializesModels;
use Webmagic\Notifier\Contracts\Notifiable;

class BaseEvent implements Notifiable
{
    use SerializesModels;

    /** @var array  */
    public $data;

    /**
     * Create a new event instance.
     *
     * @param $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }


    /**
     * Special fields with data for notifier
     *
     * @return array
     */
    public function getFieldsData(): array
    {
        return array_dot($this->data['data']);
    }

    /**
     * Fields (names) that can be used in admin panel
     *
     * @return array
     */
    public static function getAvailableFields(): array
    {
        return [];
    }


    public static function getAvailableFiles(): array
    {
        return [];
    }
}