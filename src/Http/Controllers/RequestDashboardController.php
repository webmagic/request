<?php

namespace Webmagic\Request\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Request\RequestService;

class RequestDashboardController extends BaseController
{
    /**
     * @var Dashboard
     */
    private $dashboard;

    /**
     * DashboardController constructor.
     * @param Dashboard $dashboard
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Show list of requests
     *
     * @param RequestService $requestService
     * @return Dashboard
     */
    public function index(RequestService $requestService)
    {
        $types = $requestService->typeGetAll();

        (new TablePageGenerator($this->dashboard->page()))
            ->title(__('request::common.requests'))
            ->tableTitles(['ID', __('request::common.name'), __('request::common.requests-count'), ''])
            ->showOnly(['id', 'name', 'requests_count'])
            ->setConfig([
                'requests_count' => function ($type) {
                    return count($type['requests']);
                }])
            ->items($types)
            ->setShowLinkClosure(function ($type){
                return route('request::show', $type);
            })
//            ->setEditLinkClosure(function ($type){
//                route('request::export', $type);
//            })
            ->setDestroyLinkClosure(function ($type){
                return route('request::delete_by_type', $type);
            })
        ;


        return $this->dashboard;
    }

    /**
     * Show request with fields and data by type id
     *
     * @param $type_id
     * @param RequestService $requestService
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($type_id, RequestService $requestService)
    {
        if (! $type = $requestService->typeGetByID($type_id)) {
            return response('Request type not found', 404);
        }
        $requests = $requestService->requestGetByTypeID($type_id, true);

        $fields = $requestService->fieldGetByTypeID($type_id);

        $requests = $requestService->generateRequestsCollectionWithFields($requests, $fields);

        $column_names = $type->fields->pluck('name', 'id');

        $spam_field = config('webmagic.request.spam_field');

        $content = view('request::request.request', compact('type', 'requests', 'spam_field', 'column_names'));

        return $this->getDashboardWithContent($content);
    }

    /**
     * Edit data
     *
     * @param $id
     * @param Request $request
     * @param RequestService $requestService
     */
    public function edit($id, Request $request, RequestService $requestService)
    {
        $request = $request->all();

        $requestService->requestUpdate($id, $request);
    }

    /**
     * Delete selective request
     *
     * @param $req_id
     * @param RequestService $requestService
     * @return mixed
     */
    public function destroy($req_id, RequestService $requestService)
    {
        if (! $requestService->requestDestroy($req_id)) {
            return response('При удалении возникли ошибки', 500);
        }
    }

    /**
     * Delete all requests in type
     *
     * @param $type_id
     * @param RequestService $requestService
     * @return mixed
     */
    public function destroyByTypeID($type_id, RequestService $requestService)
    {
        if (! $requestService->requestDestroyByTypeID($type_id)) {
            return response('При удалении возникли ошибки', 500);
        }
    }

    /**
     * Export request to excel by type id
     *
     * @param $type_id
     * @param RequestService $requestService
     */
    public function export($type_id, RequestService $requestService)
    {
        if (! $type = $requestService->typeGetByID($type_id)) {
            return response('Request type not found', 404);
        }
        $fields = $requestService->fieldGetForSelectByTypeID($type_id, 'name', 'id');
        $requests = $requestService->requestGetByTypeID($type_id, true);
        $requestService->exportToExcel($type, $requests, $fields);
    }

    /**
     * Prepare Dashboard and set content inside
     *
     * @param $content
     * @return mixed
     */
    public function getDashboardWithContent($content)
    {
        $dashboard = app()->make(\Webmagic\Dashboard\Dashboard::class);
        $dashboard->content($content);

        return $dashboard->render();
    }
}
