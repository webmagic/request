<?php

namespace Webmagic\Request\Http\Controllers;


use Illuminate\Routing\Controller as BaseController;
use Webmagic\Request\RequestService;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Webmagic\Request\SpamChecker\SpamChecker;

class RequestController extends BaseController
{
    use ValidatesRequests;

    /** @var SpamChecker */
    protected $spamChecker;

    /**
     * RequestController constructor.
     *
     * @param SpamChecker $spamChecker
     */
    public function __construct(SpamChecker $spamChecker)
    {
        $this->spamChecker = $spamChecker;
    }


    /**
     * Creating request
     *
     * @param $req_type
     * @param RequestService $requestService
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function create($req_type, RequestService $requestService, Request $request)
    {
        if(!$request_type = $requestService->typeGetBySlug($req_type, true)){
            return response('Тип заявки не найден', 404);
        }

        //Get rules and validate
        $rules = $requestService->getRulesForRequestByType($request_type->id);
        $request->validate($rules);

        $data = $request->all();

        //Checking for spam
        $isSpam = $this->spamChecker->isSpam($request);

        //Creating request
        if(!$request = $requestService->requestCreate([
            'req_type_id' => $request_type->id,
            'spam' => $isSpam
        ])){
            return response('При создании заявки возникли ошибки', 500);
        }

        //Saving data from fields from request
        $requestService->saveItems($request->id, $request_type->id, $data);

        $request = $requestService->requestGetByID($request->id);

        $requestService->fireEvent($request_type->id, $request->present()->asArray());

        return response()->json(['id' => $request->id]);
    }
}