<?php

namespace Webmagic\Request\Http\Controllers;


use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Webmagic\Dashboard\Components\FormGenerator;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TableGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Request\RequestService;
use Webmagic\Request\Http\Requests\TypeRequest as Request;

class RequestTypeDashboardController extends BaseController
{
    use ValidatesRequests;

    /**
     * @var Dashboard
     */
    private $dashboard;

    /**
     * DashboardController constructor.
     * @param Dashboard $dashboard
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Show list of request types
     *
     * @param RequestService $requestService
     * @return Dashboard
     */
    public function index(RequestService $requestService)
    {
        $types = $requestService->typeGetAll();

        (new TablePageGenerator($this->dashboard->page()))
            ->title(__('request::common.requests'))
            ->tableTitles([__('request::common.id'), __('request::common.name'), __('request::common.request-creation-url'), ''])
            ->showOnly(['id', 'name', 'url'])
            ->setConfig([
                'url' => function ($type) {
                    return $type->present()->requestURL();
                }
            ])
            ->items($types)
            ->setEditLinkClosure(function ($type){
                return route('request_type::edit', $type);
            })
            ->setDestroyLinkClosure(function ($type){
                return route('request_type::destroy', $type);
            })
            ->createLink(route('request_type::create'))
        ;


        return $this->dashboard;
    }


    /**
     * Show form for creating type
     *
     * @param RequestService $requestService
     * @return FormPageGenerator
     * @throws \Exception
     */
    public function create(RequestService $requestService)
    {
        $events = $requestService->findEvents();

        $formPageGenerator = (new FormPageGenerator())
            ->title(__('request::common.request-type-creation'))
            ->action(route('request_type::store'))
            ->method('POST')
            ->ajax(true)
            ->switcher('active', true, __('request::common.status'))
            ->textInput('name', null,  __('request::common.name'), true)
            ->textInput('slug', null, __('request::common.slug-input'), true)
            ->textarea('description', null,  __('request::common.description'))
            ->select('event', $events, null, __('request::common.event-select'))
            ->submitButtonTitle(__('notifier::common.save'))
        ;

        return $formPageGenerator;
    }


    /**
     * Store type
     *
     * @param Request $request
     * @param RequestService $requestService
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, RequestService $requestService)
    {
        if(!isset($request['slug']) || empty($request['slug'])){
            $request['slug'] = $requestService->slugGenerate($request['name']);
        }

        if (!$type = $requestService->typeCreate($request->all()))
            return response('Error on create', 500);
    }


    /**
     * Show form for creating with information
     *
     * @param $type_id
     * @param RequestService $requestService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|Dashboard
     * @throws \Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined
     */
    public function edit($type_id, RequestService $requestService)
    {
        if (!$type = $requestService->typeGetByID($type_id)) {
            return response('Request type not found', 500);
        }

        $events = $requestService->findEvents();

        $form = (new FormGenerator())
            ->action(route('request_type::update', $type['id']))
            ->method('PUT')
            ->ajax(true)
            ->switcher('active', $type['active'], __('request::common.status'))
            ->textInput('name', $type['name'],  __('request::common.name'))
            ->textInput('slug', $type['slug'], __('request::common.slug-input'))
            ->textarea('description', $type['description'],  __('request::common.description'))
            ->select('event', $events, $type['event'], __('request::common.event-select'))
            ->submitButton(__('notifier::common.save'))
        ;

        $table = (new TableGenerator())
            ->tableTitles([__('request::common.id'), __('request::common.name'), __('request::common.type'), __('request::common.validation-rules'), ''])
            ->showOnly(['id', 'name', 'type', 'rules'])
            ->items($type['fields'])
            ->setEditLinkClosure(function ($field){
                return route('request_field::edit', $field);
            })
            ->setDestroyLinkClosure(function ($field){
                return route('request_field::destroy', $field);
            })
        ;

        $this->dashboard->page()->setPageTitle(__('request::common.request-type-edition'))
            ->addElement()->box()
            ->content($form->render())
            ->parent('page')
            ->addElement()->box()
            ->boxTitle(__('request::common.request-available-fields'))
            ->addToolsLinkButton(route('request_field::create', $type['id']), __('request::common.add'), 'fa-plus')
            ->content($table->render())
        ;

        return $this->dashboard;
    }


    /**
     * Update type
     *
     * @param $type_id
     * @param Request $request
     * @param RequestService $requestService
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($type_id, Request $request, RequestService $requestService)
    {
        if(!isset($request['slug']) || empty($request['slug'])){
            $request['slug'] = $requestService->slugGenerate($request['name']);
        }

        $this->validate($request, [
            'slug' => 'unique:req_types,slug,'.$type_id,
        ]);

        if(!$requestService->typeUpdate($type_id, $request->all())){
            return response('Error on update', 500);
        }
    }


    /**
     * Destroy type, fields
     *
     * @param $id
     * @param RequestService $requestService
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id, RequestService $requestService)
    {
        if (!$temp = $requestService->typeDestroy($id))
            return response('Error on destroy', 500);
    }
}
