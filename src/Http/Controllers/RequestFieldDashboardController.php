<?php

namespace Webmagic\Request\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Webmagic\Dashboard\Components\FormGenerator;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Request\RequestService;
use Webmagic\Request\Http\Requests\FieldRequest as Request;

class RequestFieldDashboardController extends BaseController
{
    use ValidatesRequests;

    /**
     * Show form for creating fields in type
     *
     * @param $type_id
     * @return FormPageGenerator
     * @throws \Exception
     */
    public function create($type_id)
    {
        $field_types = config('webmagic.dashboard.request.types');
        $field['req_type_id'] = $type_id;

        $form = (new FormPageGenerator())
            ->title(__('request::common.field-creation'))
            ->action(route('request_field::store'))
            ->method('POST')
            ->ajax(true)
            ->textInput('name', null,  __('request::common.name'), true)
            ->select('type', $field_types, null, __('request::common.type'), true)
            ->textInput('rules', null,  __('request::common.validation-rules'), true)
            ->submitButtonTitle(__('notifier::common.save'))
        ;

        $form->getForm()->content()
            ->addElement()->input([
                'name' => 'req_type_id',
                'type' => 'hidden',
                'value' => $field['req_type_id']
            ])
        ;

        return $form;
    }

    /**
     * Creating fields in type
     *
     * @param Request $request
     * @param RequestService $requestService
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, RequestService $requestService)
    {
        if (! $requestService->fieldCreate($request->all())) {
            return response('Error on create', 500);
        }
    }

    /**
     * Show form for creating with information
     *
     * @param $field_id
     * @param RequestService $requestService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|FormPageGenerator
     * @throws \Exception
     */
    public function edit($field_id, RequestService $requestService)
    {
        if (! $field = $requestService->fieldGetByID($field_id)) {
            return response('Request field not found', 404);
        }

        $field_types = config('webmagic.dashboard.request.types');

        $form = (new FormPageGenerator())
            ->title(__('request::common.field-edition'))
            ->action(route('request_field::update', $field['id']))
            ->method('PUT')
            ->ajax(true)
            ->textInput('name', $field['name'],  __('request::common.name'), true)
            ->select('type', $field_types, null, __('request::common.type'), true)
            ->textInput('rules', $field['rules'],  __('request::common.validation-rules'), true)
            ->submitButtonTitle(__('notifier::common.save'))
        ;

        $form->getForm()->content()
            ->addElement()->input([
                'name' => 'req_type_id',
                'type' => 'hidden',
                'value' => $field['req_type_id']
            ])
        ;

        return $form;
    }

    /**
     * Update type and fields
     *
     * @param $id
     * @param Request $request
     * @param RequestService $requestService
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($id, Request $request, RequestService $requestService)
    {
        if (! $requestService->fieldUpdate($id, $request->all())) {
            return response('Update error', 500);
        }
    }

    /**
     * Destroy type, fields and drop table
     *
     * @param $id
     * @param RequestService $requestService
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id, RequestService $requestService)
    {
        if (! $temp = $requestService->fieldDestroy($id)) {
            return response('Error on destroy', 500);
        }
    }
}
