<?php

namespace Webmagic\Request;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Webmagic\Request\Request\RequestRepo;
use Webmagic\Request\Request\RequestRepoContract;
use Webmagic\Request\RequestField\RequestFieldRepo;
use Webmagic\Request\RequestField\RequestFieldRepoContract;
use Webmagic\Request\RequestItem\RequestItemRepo;
use Webmagic\Request\RequestItem\RequestItemRepoContract;
use Webmagic\Request\RequestType\RequestTypeRepo;
use Webmagic\Request\RequestType\RequestTypeRepoContract;
use Faker\Generator;
use Illuminate\Database\Eloquent\Factory;

class RequestServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/request.php', 'webmagic.request'
        );

        //Service registering
        $this->app->singleton('RequestService', function($app){
            return new RequestService();
        });

        $this->registerServices();
        $this->registerFactories();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        //Migrations publishing
        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('/migrations'),
        ], 'migrations');

        $this->publishes([
            __DIR__.'/config/request.php' => config_path('/webmagic/request.php'),
        ], 'config');

        //Load Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'request');

        //Load Routs
        $this->loadRoutesFrom(__DIR__ . '/routes/routes.php');

        $this->registeringMiddleware($router);
        $this->loadMigrations();
    }


    /**
     * Registering request services
     */
    private function registerServices()
    {
        $this->app->singleton(RequestTypeRepoContract::class, RequestTypeRepo::class);
        $this->app->singleton(RequestFieldRepoContract::class, RequestFieldRepo::class);
        $this->app->singleton(RequestRepoContract::class, RequestRepo::class);
        $this->app->singleton(RequestItemRepoContract::class, RequestItemRepo::class);
    }


    /**
     * Middleware registration
     *
     * @param $router
     */
    protected function registeringMiddleware($router)
    {
        $router->middlewareGroup('request', [
            \Illuminate\Cookie\Middleware\EncryptCookies::class,
            \Illuminate\Session\Middleware\StartSession::class,
        ]);
    }



    /**
     * Factories registration
     */
    protected function registerFactories()
    {
        $this->app->singleton(Factory::class, function ($app){
            return Factory::construct($app->make(Generator::class),  __DIR__.'/database/factories/');
        });
    }

    /**
     * Load migrations for module
     */
    protected function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations/');
    }

}