<?php
Route::group([
    'prefix' => config('webmagic.request.prefix'),
    'namespace' => '\Webmagic\Request\Http\Controllers',
    'as' => 'request-api::',
    'middleware' => config('webmagic.request.middleware')
],
    function() {

        /**
         * Route for sending request by type
         */
        Route::group([
            'as' => 'create',
        ], function () {
            Route::post('create/{req_type}', 'RequestController@create');
        });

    }
);