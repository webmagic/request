<?php

Route::group([
    'namespace' => '\Webmagic\Request\Http\Controllers',
    'prefix' => config('webmagic.dashboard.request.prefix'),
    'middleware' => config('webmagic.dashboard.request.middleware')
],
    function(){


        /**
         * Types routs
         */
        Route::group([
            'as' => 'request_type::',
        ], function () {

            //type control
            Route::resource('type', 'RequestTypeDashboardController',
                ['except' => ['show'],
                    'names' => [
                        'index' => 'index',
                        'create' => 'create',
                        'store' => 'store',
                        'edit' => 'edit',
                        'update' => 'update',
                        'destroy' => 'destroy'
                    ]]
            );
        });

        /**
         * Fields routs
         */
        Route::group([
            'as' => 'request_field::',
        ], function () {

            //field control
            Route::resource('field', 'RequestFieldDashboardController',
                ['except' => ['show', 'create', 'index'],
                    'names' => [
                        'store' => 'store',
                        'update' => 'update',
                        'edit' => 'edit',
                        'destroy' => 'destroy'
                    ]]
            );

            //create field for type
            Route::get('field/create/{type_id}', [
                'as' => 'create',
                'uses' => 'RequestFieldDashboardController@create'
            ]);
        });

        /**
         * Requests routs
         */
        Route::group([
            'as' => 'request::',
        ], function () {
            //show all types of requests and amounts
            Route::get('/', [
                'as' => 'index',
                'uses' => 'RequestDashboardController@index'
            ]);

            Route::post('{id}/edit', [
                'as' => 'edit',
                'uses' => 'RequestDashboardController@edit'
            ]);

            //show requests by type
            Route::get('{type_id}/show', [
                'as' => 'show',
                'uses' => 'RequestDashboardController@show'
            ]);

            //export requests by type
            Route::get('{type_id}/export', [
                'as' => 'export',
                'uses' => 'RequestDashboardController@export'
            ]);

            //destroy request by id
            Route::delete('{request_id}', [
                'as' => 'delete',
                'uses' => 'RequestDashboardController@destroy'
            ]);

            //destroy requests by type
            Route::delete('{type_id}/delete', [
                'as' => 'delete_by_type',
                'uses' => 'RequestDashboardController@destroyByTypeID'
            ]);
        });
    });



